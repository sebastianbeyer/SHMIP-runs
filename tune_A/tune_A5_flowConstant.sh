#!/bin/bash

variable=flowConstant
values="1e-25,5e-25,9e-25,15e-25"
values="1e-25,2e-25,3e-25,4e-25,5e-25,6e-25,7e-25,8e-25"
mkdir -p ~/playground/SHMIP-runs/tune_A/${variable}

cd ~/playground/CUAS

./parallelRun.py \
~/playground/SHMIP-runs/exp_A_geoms/exp_A5.nc \
    ~/playground/SHMIP-runs/tune_A/${variable}/ \
    --noflownetcdf \
    --dochannels \
    --totaltime '20years' \
    --permeability 0.02 \
    --KChanMax 0.8 \
    --${variable} ${values}

cd -
./compare_multi.py ./${variable}/ $variable --exp 5
