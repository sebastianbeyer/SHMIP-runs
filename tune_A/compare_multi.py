#!/usr/bin/env python

import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
from netCDF4 import Dataset
import argparse
import sys
import os

parser = argparse.ArgumentParser(description='Compare CUAS output to GlaDS tuning values.')
parser.add_argument('directory', metavar='Netcdf_input_dir', type=str)
parser.add_argument('variables', metavar='variable', type=str)
parser.add_argument('--exp', type=int, default=5, help="Experiment, currently 3 or 5.")
args = parser.parse_args()

directory = args.directory

varlist = args.variables.split(',')


if (args.exp == 3):
    GLADS_DATA = '../GLADStuning/tuning_A3'
elif (args.exp == 5):
    GLADS_DATA = '../GLADStuning/tuning_A5'
else:
    print('only experiment A3 or A5 supported.')
    sys.exit(1)


gdata = np.loadtxt(GLADS_DATA, delimiter=',', skiprows=1)

gx = gdata[:,0]
gNmean = gdata[:,1]
gNmin = gdata[:, 2]
gNmax = gdata[:,3]
gqmean = gdata[:,4]
gQmax = gdata[:,7]


def collapsemean(twodarr):
    mean = np.zeros(twodarr.shape[1])
    for i in range(twodarr.shape[1]):
        mean[i] = np.nanmean(twodarr[1:-1,i])
    return mean


def readNetcdf(netcdf, variables):
    ncdata = Dataset(netcdf, mode='r')
    print(netcdf)
    ncx = ncdata.variables['x'][:]
    ncy = ncdata.variables['y'][:]
    ncN = ncdata.variables['peffective'][:]
    ncerr = ncdata.variables['eps_inf'][:]
    ncK = ncdata.variables['kout'][:]
    ncq = ncdata.variables['flux'][:]
    ncKtime = ncdata.variables['permeability_t'][:]

    ncvariables = []
    for variable in variables:
        ncvariables.append(ncdata.getncattr(variable))

    ncdata.close()

    ncNmean = collapsemean(ncN)
    ncKMean = collapsemean(ncK)
    ncqmean = collapsemean(ncq)

    nx = len(ncx)
    ncKmeantime = np.empty((nx, 100))
    for i in range(0,99):
        ncKmeantime[:,i] = collapsemean(ncKtime[i,:,:])

    print(ncvariables)
    print(np.mean(ncK[1:-2,1:-2]))

    return ncx, ncNmean, ncerr, ncKMean, ncKmeantime, ncqmean, ncvariables

def shortnames(names):
    return '-'.join([name[:3] for name in names.split(',')])

def numlist2str(numlist):
    thelist = []
    for num in numlist:
        thelist.append('{:.1e}'.format(num))
        # thelist.append(num)
    return '-'.join(thelist)

print(directory)

##### plots ####
fig = plt.figure(figsize=(12, 15))

# Axes that share the x-axis
ax = fig.add_subplot(5, 1, 1)
axes = [ax] + [fig.add_subplot(5, 1, i, sharex=ax) for i in range(2, 5)]

# The bottom independent axes
axes.append(fig.add_subplot(5, 1, 5))

# Let's hide the tick labels for all but the last shared-x axes
for ax in axes[:1]:
    plt.setp(ax.get_xticklabels(), visible=False)

axes[1].plot(gx/1000, gNmean/1e6, label='GlaDS', lw=1.5, color='k')
# axes[2].fill(gqmean, gQmax, color='k', alpha=0.3)
axes[3].plot(gx/1000, gqmean, label='GlaDS', lw=1.5, color='k')
# axes[2].plot(gx/1000, gQmax, label='GlaDS', lw=1.5, color='k')


def nfiles(directory):
    ''' computes number of nc files in dir
    '''
    i = 0
    for filename in sorted(os.listdir(directory)):
        if filename.endswith('.nc'):
            i = i + 1
    return i

color=iter(cm.rainbow(np.linspace(0,1,nfiles(directory))))
sizes = np.linspace(6,1,num=nfiles(directory))
i = 0
for filename in sorted(os.listdir(directory)):
    if (filename.endswith('.nc') and "exp" in filename):
        print(filename)
        ncx, ncNmean, ncerr, ncKmean, ncKmeantime, ncqmean, ncvariables = readNetcdf(directory+'/'+filename, varlist)
        label = shortnames(args.variables) + ' ' + str(ncvariables)
        # label = 'E'+str(i)
        label = shortnames(args.variables) + ' ' + numlist2str(ncvariables)
        c = next(color)
        # axes[0].plot(ncx[:-1]/1000, ncNmean[:-1]/1e6, '--', color=c, label=label, lw=1.4)
        axes[1].plot(ncx[:-1]/1000, ncNmean[:-1]/1e6, '--', color=c, label=label, lw=sizes[i])
        # axes[2].plot(ncx[:-1]/1000, ncKmean[:-1], '--', color=c, lw=sizes[i])
        axes[2].plot(ncx[:-1]/1000, ncKmean[:-1], color=c, lw=sizes[i])
        axes[3].plot(ncx[:-1]/1000, ncqmean[:-1], '--', color=c, lw=sizes[i])
        axes[4].plot(ncerr, color=c, lw=sizes[i])
        i=i+1
        print('ncvar:', ncvariables)
        if 0.1 in ncvariables:
            print('jojo')
            N01 = ncNmean[:-1]
            x01 = ncx[:-1]
        if 8.0 in ncvariables:
            print('jaja')
            N8 = ncNmean[:-1]
            x8 = ncx[:-1]
# axes[0].plot(x01/1000, N01/1e6, '--', label='0.1')
# axes[0].plot(x8/1000, N8/1e6, '--', label='8')

# axes[0].plot(x8/1000, (N01-N8)/1e6, '--', label='0.1 - 8.0')
# axes[0].legend(loc='center left', bbox_to_anchor=(1, 0.5))
# axes[0].set_ylabel('N (MPa)')

box = axes[0].get_position()
axes[0].set_position([box.x0, box.y0, box.width * 0.8, box.height])
box = axes[1].get_position()
axes[1].set_position([box.x0, box.y0, box.width * 0.8, box.height])
box = axes[2].get_position()
axes[2].set_position([box.x0, box.y0, box.width * 0.8, box.height])
box = axes[3].get_position()
axes[3].set_position([box.x0, box.y0, box.width * 0.8, box.height])
box = axes[4].get_position()
axes[4].set_position([box.x0, box.y0, box.width * 0.8, box.height])
axes[1].legend(loc='center left', bbox_to_anchor=(1, 0.5))
axes[1].set_ylabel('N (MPa)')
# axes[0].set_title(directory)
titleString = __file__.replace('_', '_')+'\n' + directory
axes[1].set_title(titleString)
# axes[0].set_ylim([-1,3])
axes[2].set_ylabel('K (m/s)')
axes[2].set_yscale('log')
# axes[1].set_ylim([0,0.06])
axes[3].set_xlabel('x (km)')
axes[3].set_ylabel('q (m^2/s)')
axes[4].set_ylabel('Error (m)')
axes[4].set_xlabel('timestep')
axes[4].set_yscale('log')
plt.savefig('./compare_GlaDS_'+args.variables+'.png')


## single K plots

