#!/bin/bash

variable=layerThickness
values="0.1,1,2,5,10,15,20"
values="8,9,11,12"
values="4,5,6,7,13,14,15,16"
mkdir -p ~/playground/SHMIP-runs/tune_A/${variable}

cd ~/playground/CUAS

./parallelRun.py \
~/playground/SHMIP-runs/exp_A_geoms/exp_A5.nc \
    ~/playground/SHMIP-runs/tune_A/${variable}/ \
    --noflownetcdf \
    --dochannels \
    --totaltime '20years' \
    --permeability 0.02 \
    --KChanMax 0.8 \
    --${variable} ${values}

cd -
./compare_multi.py ./${variable}/ $variable --exp 5
