#!/bin/bash

variable1=flowConstant
values1="1e-25,8e-25"
variable2=supplymultiplier
values2="1,0.5,2,1.5"
values2="0.1,0.2,4,8"
values2="0.1,0.2,4,8"
values2="0.01,0.001,100,1000"
mkdir -p ~/playground/SHMIP-runs/tune_A/supplysensitivity

cd ~/playground/CUAS

./parallelRun.py \
~/playground/SHMIP-runs/exp_A_geoms/exp_A5.nc \
    ~/playground/SHMIP-runs/tune_A/supplysensitivity \
    --noflownetcdf \
    --dochannels \
    --totaltime '20years' \
    --permeability 0.02 \
    --KChanMax 0.5 \
    --${variable1} ${values1} \
    --${variable2} ${values2}

cd -
./compare_multi.py ./supplysensitivity/ $variable2 --exp 5
