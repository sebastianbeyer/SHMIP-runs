#!/bin/bash

##
## this tunes A3
##

mkdir -p ~/playground/SHMIP-runs/results/A3tuning2

cd ~/playground/CUAS

./parallelRun.py \
~/playground/SHMIP-runs/exp_A_geoms/exp_A3.nc \
    ~/playground/SHMIP-runs/results/A3tuning2/ \
    --noflownetcdf \
    --downsample 1 \
    --nt 1000000 \
    --permeability 0.0032,0.0034,0.0036,0.0038 \
    --Nfac 0.1e7 \
    --KChanMax 0.03

    # --dochannels \
cd -
./compare_multi.py ./results/A3tuning2/ permeability --exp 3
