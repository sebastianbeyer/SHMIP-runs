#!/bin/bash

variable=latentHeat
values="0.34e5,1.34e5,2.34e5,3.34e5,4.34e5,5.34e5,6.34e5,7.35e5"
mkdir -p ~/playground/SHMIP-runs/tune_A/${variable}

cd ~/playground/CUAS

./parallelRun.py \
~/playground/SHMIP-runs/exp_A_geoms/exp_A5.nc \
    ~/playground/SHMIP-runs/tune_A/${variable}/ \
    --noflownetcdf \
    --dochannels \
    --totaltime '20years' \
    --permeability 0.02 \
    --KChanMax 0.8 \
    --${variable} ${values}

cd -
./compare_multi.py ./${variable}/ $variable --exp 5
