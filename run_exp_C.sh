#!/bin/bash

CUASpath="$HOME/playground/CUAS"

mkdir -p ./results/C
mkdir -p ./results/C_plots

"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --dochannels \
    --restart ./results/B/exp_B5_result.nc \
    --totaltime 1year \
    --ntout 8064 \
    --permeability 0.1 \
    --KChanMax 1.0 \
    --layerThickness 10 \
    ./exp_C_geoms/exp_C1.nc \
    ./results/C/exp_C1_result.nc



# compare plot
# ./compare_GlaDS.py results/A/exp_A5_result.nc results/A_plots --exp 5
