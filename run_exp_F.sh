#!/bin/bash

CUASpath="$HOME/playground/CUAS"
doinitial=true

mkdir -p ./results/F
mkdir -p ./results/F_plots

if [ "$doinitial" = true ] ; then
    echo "doing initialization"
    "${CUASpath}"/runCUAS.py \
        --noflownetcdf \
        --dochannels \
        --totaltime 10years \
        --ntout 100 \
        --permeability 0.02 \
        --KChanMax 1.0 \
        ./exp_F_geoms/exp_F_initial.nc \
        ./results/F/exp_F_initial_result.nc
fi


"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --dochannels \
    --restart ./results/F/exp_F_initial_result.nc \
    --totaltime 10years \
    --ntout 3650 \
    --permeability 0.02 \
    --KChanMax 1.0 \
    ./exp_F_geoms/exp_F1.nc \
    ./results/F/exp_F1_result.nc
