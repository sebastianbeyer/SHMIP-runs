#!/usr/bin/env python

import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
from netCDF4 import Dataset
import argparse
import sys
import os

path="./"

GLADS_DATA_A3 = '../../GLADStuning/tuning_A3'
GLADS_DATA_A5 = '../../GLADStuning/tuning_A5'
gdata3 = np.loadtxt(GLADS_DATA_A3, delimiter=',', skiprows=1)
gdata5 = np.loadtxt(GLADS_DATA_A5, delimiter=',', skiprows=1)
gx = gdata3[:,0]
gNmean3 = gdata3[:,1]
gNmin3 = gdata3[:, 2]
gNmax3 = gdata3[:,3]
gQmax3 = gdata3[:,7]
gNmean5 = gdata5[:,1]
gNmin5 = gdata5[:, 2]
gNmax5 = gdata5[:,3]
gQmax5 = gdata5[:,7]


def collapsemean(twodarr):
    mean = np.zeros(twodarr.shape[1])
    for i in range(twodarr.shape[1]):
        mean[i] = np.mean(twodarr[1:-1,i])
    return mean

def readNetcdf(netcdf):
    ncdata = Dataset(netcdf, mode='r')
    ncx = ncdata.variables['x'][:]
    ncy = ncdata.variables['y'][:]
    ncN = ncdata.variables['peffective'][:]
    nceps = ncdata.variables['eps_L2'][:]
    ncK = ncdata.variables['kout'][:]
    ncq = ncdata.variables['flux'][:]
    ncdata.close()
    ncNmean = collapsemean(ncN)
    ncKMean = collapsemean(ncK)
    ncqmean = collapsemean(ncq)
    nx = len(ncx)
    return ncx, ncNmean, nceps, ncKMean, ncqmean

##### plots ####
f, axarr = plt.subplots(2, sharex=True, figsize=(12,6))
axarr[0].set_title('exp A')
axarr[0].plot(gx/1000, gNmean3/1e6,       label='GlaDS A3', lw=1.5, color='k')
axarr[0].plot(gx/1000, gNmean5/1e6, '--', label='GlaDS A5', lw=1.5, color='k')
for i in range(1,7):
    print(i)
    ncx, ncNmean, nceps, ncKmean, ncqmean = readNetcdf(path+'exp_A'+str(i)+'_result.nc')
    axarr[0].plot(ncx[:-1]/1000, ncNmean[:-1]/1e6, label='A'+str(i), lw=1.4)
    axarr[1].plot(ncx[:-1]/1000, ncKmean[:-1], lw=1.4)

axarr[0].legend(loc='upper left', ncol=8)
axarr[0].set_ylabel('N (MPa)')
# axarr[0].set_ylim([0,3.5])
axarr[1].set_ylabel('K (m2/s)')
axarr[1].set_xlabel('x (km)')

plt.savefig('./results_A.png')

