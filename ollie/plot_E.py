#!/usr/bin/env python

import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
from netCDF4 import Dataset
import argparse
import sys
import os

path="./"



def collapsemean(twodarr):
    mean = np.zeros(twodarr.shape[1])
    for i in range(twodarr.shape[1]):
        mean[i] = np.mean(twodarr[1:-1,i])
    return mean

def readNetcdf(netcdf):
    ncdata = Dataset(netcdf, mode='r')
    ncx = ncdata.variables['x'][:]
    ncy = ncdata.variables['y'][:]
    ncN = ncdata.variables['peffective'][:]
    nceps = ncdata.variables['eps_L2'][:]
    ncK = ncdata.variables['kout'][:]
    ncq = ncdata.variables['flux'][:]

    noflow = ncdata.variables['noflow'][:]
    ncdata.close()

    rows, cols = np.where(noflow==1)
    ncN[rows,cols] = np.nan
    ncK[rows,cols] = np.nan
    ncq[rows,cols] = np.nan
    ncNmean = collapsemean(ncN)
    ncKMean = collapsemean(ncK)
    ncqmean = collapsemean(ncq)
    nx = len(ncx)
    return ncx, ncNmean, nceps, ncKMean, ncqmean

##### plots ####
f, axarr = plt.subplots(2, sharex=True, figsize=(12,6))
axarr[0].set_title('exp E')
for i in range(1,5):
    print(i)
    ncx, ncNmean, nceps, ncKmean, ncqmean = readNetcdf(path+'exp_E'+str(i)+'_result.nc')
    axarr[0].plot(ncx[:-1]/1000, ncNmean[:-1]/1e6, label='E'+str(i), lw=1.4)
    axarr[1].plot(ncx[:-1]/1000, ncKmean[:-1], lw=1.4)

axarr[0].legend(loc='upper left', ncol=8)
axarr[0].set_ylabel('N (MPa)')
# axarr[0].set_ylim([0,3.5])
axarr[1].set_ylabel('K (m2/s)')
axarr[1].set_xlabel('x (km)')

plt.savefig('./results_E.png')

