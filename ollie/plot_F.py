#!/usr/bin/env python
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import matplotlib as mpl
import datetime

import sys
from argparse import ArgumentParser
from netCDF4 import Dataset

RHO_I = 910
RHO_W = 1000
G = 9.81

starttime = 0
endtime = starttime + 364

def spatialmean(variable, location):
    """Mean of variable, locations are
    bottom, middle and top
    """
    locations = {
        'bottom' : (10, 15),
        'middle' : (50, 55),
        'top'    : (85, 90)
        }
    return np.mean(variable[:,:,locations[location][0]:locations[location][1]], axis=(1,2))

def load_data(netCDFfile):
    ## open netcdf file
    ncin = Dataset(netCDFfile, mode='r')
    x = ncin.variables['x'][:]
    y = ncin.variables['y'][:]
    t = ncin.variables['time'][:]
    head = ncin.variables['head_t'][:]
    N = ncin.variables['peffective_t'][:]
    thk  = ncin.variables['thk'][:]
    topg = ncin.variables['topg'][:]
    K = ncin.variables['permeability_t'][:]
    opening = ncin.variables['opening_t'][:]
    closure = ncin.variables['closure_t'][:]
    eps_L2 = ncin.variables['eps_L2'][:]
    eps_inf = ncin.variables['eps_inf'][:]
    supply = ncin.variables['watersource_t'][:]
    ncin.close()
    nt = head.shape[0]
    ny = head.shape[1]
    nx = head.shape[2]
    xmin= x[0]
    xmax= x[-1]
    ymin= y[0]
    ymax= y[-1]
    spy = 60 * 60 * 24 * 365
    t_days = t / (60 * 60 * 24)
    Nlongterm = spatialmean(N, 'bottom') / 1e6
    N_single = N[starttime:endtime,:,:]
    t_single = t[starttime:endtime]
    t_single_days = t_single / (60*60*24)
    N_single_bottom = spatialmean(N_single, 'bottom') / 1e6
    N_single_middle = spatialmean(N_single, 'middle') / 1e6
    N_single_top = spatialmean(N_single, 'top') / 1e6
    # K
    K_single = K[starttime:endtime,:,:]
    K_single_bottom = spatialmean(K_single, 'bottom')
    K_single_middle = spatialmean(K_single, 'middle')
    K_single_top = spatialmean(K_single, 'top')
    supply_single = supply[starttime:endtime,:,:]
    supply_single = spatialmean(supply_single, 'bottom')
    return t_days, Nlongterm, eps_inf, eps_L2, t_single_days, N_single_bottom, N_single_middle, N_single_top, K_single_bottom, K_single_middle, K_single_top, supply_single

plt.figure(figsize=(20,20))
ax1  = plt.subplot2grid((7, 3), (0, 0),  )
ax2  = plt.subplot2grid((7, 3), (0, 1),  )
ax3  = plt.subplot2grid((7, 3), (1, 0), colspan=2)
ax4  = plt.subplot2grid((7, 3), (2, 0), colspan=2)
ax5  = plt.subplot2grid((7, 3), (3, 0), colspan=2)
ax6  = plt.subplot2grid((7, 3), (4, 0), colspan=2)
ax7  = plt.subplot2grid((7, 3), (5, 0),  )
ax8  = plt.subplot2grid((7, 3), (5, 1),  )
ax9  = plt.subplot2grid((7, 3), (6, 0),  )
ax10 = plt.subplot2grid((7, 3), (6, 1),  )

ax1.set_xlabel('time (days)')
ax1.set_ylabel('N (MPa)')
ax1.set_title('exp D')

ax2.set_ylabel('eps')
ax2.set_xlabel('time (days)')
ax2.set_yscale('log')

ax3.set_xlabel('time (days)')
ax3.set_ylabel('N (MPa) bottom')
ax3t = ax3.twinx()
ax3t.set_ylabel('K (m/s)')

ax4.set_xlabel('time (days)')
ax4.set_ylabel('N (MPa) middle')
ax4t = ax4.twinx()
ax4t.set_ylabel('K (m/s)')
ax5.set_xlabel('time (days)')
ax5.set_ylabel('N (MPa) top')
ax5t = ax5.twinx()
ax5t.set_ylabel('K (m/s)')

ax6.set_xlabel('time (days)')
ax6.set_ylabel('supply (?)')

# loop over experiments
# should be to 7 because there are 6 exps, but the last one did not finish
for i in range(1,6):
    print(i)
    t_days, Nlongterm, eps_inf, eps_L2, t_single_days, N_single_bottom, N_single_middle, N_single_top, K_single_bottom, K_single_middle, K_single_top, supply_single = load_data('./exp_F'+str(i)+'_result.nc')
    ax1.plot(t_days, Nlongterm)
    ax2.plot(t_days, eps_inf, label='eps_inf')
    ax2.plot(t_days, eps_L2, label='eps_L2')
    ax3.plot(t_single_days, N_single_bottom, label='D'+str(i))
    ax3t.plot(t_single_days, K_single_bottom, '--', )
    ax4.plot(t_single_days, N_single_middle, label='middle')
    ax4t.plot(t_single_days, K_single_middle, '--', )
    ax5.plot(t_single_days, N_single_top, label='top')
    ax5t.plot(t_single_days, K_single_top, '--', )
    ax6.plot(t_single_days, -supply_single)

# mark timeframe in first plot
ax1.axvline(t_days[starttime], c='k', alpha=0.5)
ax1.axvline(t_days[endtime], c='k', alpha=0.5)

ax2.legend()
ax3.legend(loc='lower left')

#### 2D stuff ####
lims=(-10,10)
colorsN = 'coolwarm'
normN=mpl.colors.SymLogNorm(linthresh=0.2*np.diff(lims), linscale=2,vmin=lims[0], vmax=lims[1])

# im7 = ax7.imshow(N[starttime,:,:]/1e6, norm=normN, cmap=colorsN)
ax7.set_title('N winter')
ax7.set_xlabel('x (km)')
ax7.set_ylabel('y (km)')
# plt.colorbar(im7, ax=ax7)
# im8 = ax8.imshow(N[starttime+180,:,:]/1e6, norm=normN, cmap=colorsN)
ax8.set_title('N summer')
ax8.set_xlabel('x (km)')
ax8.set_ylabel('y (km)')
# plt.colorbar(im8, ax=ax8)

lims=(0,0.01)
normK=mpl.colors.SymLogNorm(linthresh=0.2*np.diff(lims), linscale=2,vmin=lims[0], vmax=lims[1])
normK=mpl.colors.SymLogNorm(linthresh=0.05*np.diff(lims), linscale=2,vmin=lims[0], vmax=lims[1])

# im9 = ax9.imshow(K[starttime,:,:], norm=normK)
ax9.set_title('K winter')
ax9.set_xlabel('x (km)')
ax9.set_ylabel('y (km)')
# plt.colorbar(im9, ax=ax9)
# im10 = ax10.imshow(K[starttime+180,:,:], norm=normK)
ax10.set_title('K summer')
ax10.set_xlabel('x (km)')
ax10.set_ylabel('y (km)')
# plt.colorbar(im10, ax=ax10)

plt.tight_layout()
plt.savefig('./results_F.png', bbox_inches='tight')
