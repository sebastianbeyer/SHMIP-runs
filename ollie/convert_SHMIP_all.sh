#!/bin/bash

version="v0.5.0"
convert="$HOME/playground/SHMIP-runs/convert2SHMIP-format.py"
convert_C="$HOME/playground/SHMIP-runs/convert2SHMIP-format.py --reltime --starttime 7990 --endtime 8013"
convert_D="$HOME/playground/SHMIP-runs/convert2SHMIP-format.py --reltime --starttime 2900 --endtime 3264"
convert_F="$HOME/playground/SHMIP-runs/convert2SHMIP-format.py --reltime --starttime 0 --endtime 364"

for f in exp*_result.nc
do
    # echo $f
    exp=$(echo "$f" | cut -d _ -f 2)
    echo "$exp"
    exptag=$(echo "$exp" | cut -c 1)
    # if [ "$exptag" = "C"]; then
    case "$exptag" in
        C)
            $convert_C "$f" ${exp}_sbey.nc --overwriteVersion $version
            ;;
        D)
            $convert_D "$f" ${exp}_sbey.nc --overwriteVersion $version
            ;;
        F)
            $convert_F "$f" ${exp}_sbey.nc --overwriteVersion $version
            ;;
        *)
            $convert "$f" ${exp}_sbey.nc --overwriteVersion $version
    esac


done
