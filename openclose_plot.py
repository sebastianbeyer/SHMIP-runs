#!/usr/bin/env python
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import datetime

import sys
from argparse import ArgumentParser
from netCDF4 import Dataset

## Set up the option parser
parser = ArgumentParser()
parser.description = '''Animate CUAS results for channel growth'''
parser.add_argument('ncin')
parser.add_argument('result')
options = parser.parse_args()

## open netcdf file
ncin = Dataset(options.ncin, mode='r')

x = ncin.variables['x'][:]
y = ncin.variables['y'][:]
t = ncin.variables['time'][:]

RHO_I = 910
RHO_W = 1000
G = 9.81

head = ncin.variables['head_t'][:]
thk  = ncin.variables['thk'][:]
topg = ncin.variables['topg'][:]
K = ncin.variables['permeability_t'][:]
opening = ncin.variables['opening_t'][:]
closure = ncin.variables['closure_t'][:]
error = ncin.variables['err'][:]
ncin.close()


nt = head.shape[0]
ny = head.shape[1]
nx = head.shape[2]

xmin= x[0]
xmax= x[nx-1]
ymin= y[0]
ymax= y[ny-1]

pw = (head - topg) * RHO_W * G
pi = thk * RHO_I * G
pe = (pi - pw) / 1e6  # in MPa


fig, (ax1, ax2, ax3, ax4, ax5, ) = plt.subplots(5,1, figsize=[10,17])

tmin=20
tmax=99

tmin_oc=20

# day0 = datetime.datetime(2000,1,1)
# time = [day0 + datetime.timedelta(seconds=int(second)) for second in t]
# print(time)
spy = 60 * 60 * 24 * 365
time = t / spy
time = time

ax1.plot(time[tmin:tmax], K[tmin:tmax,10,98], label='up')
# ax1.plot(time[tmin:tmax], K[tmin:tmax,10,50], label='mid')
# ax1.plot(time[tmin:tmax], K[tmin:tmax,10,10], label='down')
ax2.plot(time[tmin:tmax], pe[tmin:tmax,10,98])
# ax2.plot(time[tmin:tmax], pe[tmin:tmax,10,50])
# ax2.plot(time[tmin:tmax], pe[tmin:tmax,10,10])
# ax3.plot(opening[tmin:tmax,10,98])
# ax3.plot(opening[tmin:tmax,10,50])
ax3.plot(time[tmin_oc:tmax], opening[tmin_oc:tmax,10,10])
ax3.plot(time[tmin_oc:tmax], closure[tmin_oc:tmax,10,10], '--')

# ax4.plot(time[tmin:tmax], (opening-closure)[tmin:tmax,10,98])
ax4.fill_between(time[tmin:tmax], (opening-closure)[tmin:tmax,10,98], where=(opening-closure)[tmin:tmax,10,98]<=0, color='#ff7f0e', interpolate=True)
ax4.fill_between(time[tmin:tmax], (opening-closure)[tmin:tmax,10,98], where=(opening-closure)[tmin:tmax,10,98]>=0, color='#1f77b4', interpolate=True)

# ax5.plot(time[tmin:tmax], (opening-closure)[tmin:tmax,10,50])
# ax5.plot(time[tmin:tmax], (opening-closure)[tmin:tmax,10,10])
ax5.plot(time[tmin:tmax], error[tmin:tmax])

# logarithmic
# ax3.set_yscale('log')
# ax4.set_yscale('symlog', linthreshy=1e-12)
ax4.set_yscale('symlog', linthreshy=1e-12)
ax5.set_yscale('log')

ax1.set_ylabel(r'K')
ax1.legend()
ax1.set_ylabel(r'K')
ax2.set_ylabel(r'N')
ax3.set_ylabel(r'opening / closure (dashed)')
ax4.set_ylabel(r'delta')
ax5.set_ylabel(r'error')
ax5.set_xlabel(r'year')

fig.savefig(options.result, bbox_inches='tight')
