#!/bin/bash

CUASpath="$HOME/playground/CUAS"

mkdir -p ./results/A
mkdir -p ./results/A_plots

"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --dochannels \
    --totaltime 60years \
    --ntout 100 \
    --permeability 0.02 \
    --KChanMax 0.2 \
    --layerThickness 10 \
    ./exp_A_geoms/exp_A1.nc \
    ./results/A/exp_A1_result_for_D.nc


# compare plot
# ./compare_GlaDS.py results/A/exp_A5_result.nc results/A_plots --exp 5
