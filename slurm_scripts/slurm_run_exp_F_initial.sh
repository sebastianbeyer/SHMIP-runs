#!/bin/bash

CUASpath="$HOME/CUAS"
geompath="$HOME/SHMIP-runs/exp_F_geoms"
resultpath="$HOME/SHMIP-runs/results/F"

mkdir -p $resultpath

run="${CUASpath}/runCUAS.py"
commonpars="--noflownetcdf --dochannels --downsample 5 --totaltime 90days --ntout 100 --permeability 0.02 --KChanMax 0.5 --KChanMin 0.003"

parameters=(
            "$run $commonpars $geompath/exp_F_initial.nc $resultpath/exp_F_initial_result.nc"
           )


id=$SLURM_PROCID

echo $id
echo ${parameters[id]}

python -u  ${parameters[id]}


