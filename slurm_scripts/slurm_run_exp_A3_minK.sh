#!/bin/bash

CUASpath="$HOME/CUAS"
geompath="$HOME/SHMIP-runs/exp_A_geoms"
resultpath="$HOME/SHMIP-runs/results/A5mink"

mkdir -p $resultpath

run="${CUASpath}/runCUAS.py"
commonpars="--noflownetcdf --dochannels --totaltime 50years --ntout 100 --permeability 0.02 --KChanMax 0.5"

parameters=(
            "$run $commonpars --KChanMin 0.030 $geompath/exp_A5.nc $resultpath/exp_A5_min0.030_result.nc"
            "$run $commonpars --KChanMin 0.025 $geompath/exp_A5.nc $resultpath/exp_A5_min0.025_result.nc"
            "$run $commonpars --KChanMin 0.020 $geompath/exp_A5.nc $resultpath/exp_A5_min0.020_result.nc"
            "$run $commonpars --KChanMin 0.015 $geompath/exp_A5.nc $resultpath/exp_A5_min0.015_result.nc"
            "$run $commonpars --KChanMin 0.010 $geompath/exp_A5.nc $resultpath/exp_A5_min0.010_result.nc"
            "$run $commonpars --KChanMin 0.008 $geompath/exp_A5.nc $resultpath/exp_A5_min0.008_result.nc"
            "$run $commonpars --KChanMin 0.007 $geompath/exp_A5.nc $resultpath/exp_A5_min0.007_result.nc"
            "$run $commonpars --KChanMin 0.005 $geompath/exp_A5.nc $resultpath/exp_A5_min0.005_result.nc"
            "$run $commonpars --KChanMin 0.001 $geompath/exp_A5.nc $resultpath/exp_A5_min0.001_result.nc"
           )


id=$SLURM_PROCID

echo "SLURM procid: $id"
echo ${parameters[id]}

python -u  ${parameters[id]}


