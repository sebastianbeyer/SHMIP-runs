#!/bin/bash

CUASpath="$HOME/CUAS"
geompath="$HOME/SHMIP-runs/exp_E_geoms"
resultpath="$HOME/SHMIP-runs/results/E"

mkdir -p $resultpath

run="${CUASpath}/runCUAS.py"
commonpars="--noflownetcdf --dochannels --downsample 5 --totaltime 90days --ntout 100 --permeability 0.02 --KChanMax 0.5 --KChanMin 0.003"

parameters=(
            "$run $commonpars $geompath/exp_E1.nc $resultpath/exp_E1_result.nc"
            "$run $commonpars $geompath/exp_E2.nc $resultpath/exp_E2_result.nc"
            "$run $commonpars $geompath/exp_E3.nc $resultpath/exp_E3_result.nc"
            "$run $commonpars $geompath/exp_E4.nc $resultpath/exp_E4_result.nc"
            "$run $commonpars $geompath/exp_E5.nc $resultpath/exp_E5_result.nc"
           )


id=$SLURM_PROCID

echo $id
echo ${parameters[id]}

python -u  ${parameters[id]}


