#!/bin/bash

CUASpath="$HOME/CUAS"
geompath="$HOME/SHMIP-runs/exp_D_geoms"
resultpath="$HOME/SHMIP-runs/results/D"

mkdir -p $resultpath

run="${CUASpath}/runCUAS.py"
commonpars="--noflownetcdf --restart $HOME/SHMIP-runs/results/A/exp_A1_result.nc --dochannels --totaltime 10years --ntout 3650 --permeability 0.02 --KChanMax 0.5 --KChanMin 0.003"

parameters=(
            "$run $commonpars $geompath/exp_D1.nc $resultpath/exp_D1_result.nc"
            "$run $commonpars $geompath/exp_D2.nc $resultpath/exp_D2_result.nc"
            "$run $commonpars $geompath/exp_D3.nc $resultpath/exp_D3_result.nc"
            "$run $commonpars $geompath/exp_D4.nc $resultpath/exp_D4_result.nc"
            "$run $commonpars $geompath/exp_D5.nc $resultpath/exp_D5_result.nc"
           )


id=$SLURM_PROCID

echo $id
echo ${parameters[id]}

python -u  ${parameters[id]}


