#!/bin/bash

CUASpath="$HOME/CUAS"
geompath="$HOME/SHMIP-runs/exp_A_geoms"
resultpath="$HOME/SHMIP-runs/results/A3minknew"

mkdir -p $resultpath

run="${CUASpath}/runCUAS.py"
commonpars="--noflownetcdf --dochannels --totaltime 50years --ntout 100 --permeability 0.02 --KChanMax 0.5"

parameters=(
            "$run $commonpars --KChanMin 0.0010 $geompath/exp_A3.nc $resultpath/exp_A3_min0.0010_result.nc"
            "$run $commonpars --KChanMin 0.0015 $geompath/exp_A3.nc $resultpath/exp_A3_min0.0015_result.nc"
            "$run $commonpars --KChanMin 0.0020 $geompath/exp_A3.nc $resultpath/exp_A3_min0.0020_result.nc"
            "$run $commonpars --KChanMin 0.0025 $geompath/exp_A3.nc $resultpath/exp_A3_min0.0025_result.nc"
            "$run $commonpars --KChanMin 0.0030 $geompath/exp_A3.nc $resultpath/exp_A3_min0.0030_result.nc"
            "$run $commonpars --KChanMin 0.0035 $geompath/exp_A3.nc $resultpath/exp_A3_min0.0035_result.nc"
            "$run $commonpars --KChanMin 0.0040 $geompath/exp_A3.nc $resultpath/exp_A3_min0.0040_result.nc"
            "$run $commonpars --KChanMin 0.0045 $geompath/exp_A3.nc $resultpath/exp_A3_min0.0045_result.nc"
            "$run $commonpars --KChanMin 0.0050 $geompath/exp_A3.nc $resultpath/exp_A3_min0.0050_result.nc"
           )


id=$SLURM_PROCID

echo "SLURM procid: $id"
echo ${parameters[id]}

python -u  ${parameters[id]}


