#!/bin/bash

CUASpath="$HOME/CUAS"
geompath="$HOME/SHMIP-runs/exp_F_geoms"
resultpath="$HOME/SHMIP-runs/results/F"

mkdir -p $resultpath

run="${CUASpath}/runCUAS.py"
commonpars="--noflownetcdf --downsample 5 --dochannels --restart $resultpath/exp_F_initial_result.nc --totaltime 1years --ntout 365 --permeability 0.02 --KChanMax 0.5 --KChanMin 0.003"

parameters=(
            "$run $commonpars $geompath/exp_F1.nc $resultpath/exp_F1_result.nc"
            "$run $commonpars $geompath/exp_F2.nc $resultpath/exp_F2_result.nc"
            "$run $commonpars $geompath/exp_F3.nc $resultpath/exp_F3_result.nc"
            "$run $commonpars $geompath/exp_F4.nc $resultpath/exp_F4_result.nc"
            "$run $commonpars $geompath/exp_F5.nc $resultpath/exp_F5_result.nc"
           )


id=$SLURM_PROCID

echo $id
echo ${parameters[id]}

python -u  ${parameters[id]}


