#!/bin/bash

CUASpath="$HOME/CUAS"
geompath="$HOME/SHMIP-runs/exp_C_geoms"
resultpath="$HOME/SHMIP-runs/results/C"

mkdir -p $resultpath

run="${CUASpath}/runCUAS.py"
commonpars="--noflownetcdf --restart $HOME/SHMIP-runs/results/B/exp_B5_result.nc --dochannels --totaltime 1year --ntout 8064 --permeability 0.02 --KChanMax 0.5 --KChanMin 0.003"

parameters=(
            "$run $commonpars $geompath/exp_C1.nc $resultpath/exp_C1_result.nc"
            "$run $commonpars $geompath/exp_C2.nc $resultpath/exp_C2_result.nc"
            "$run $commonpars $geompath/exp_C3.nc $resultpath/exp_C3_result.nc"
            "$run $commonpars $geompath/exp_C4.nc $resultpath/exp_C4_result.nc"
           )


id=$SLURM_PROCID

echo $id
echo ${parameters[id]}

python -u  ${parameters[id]}


