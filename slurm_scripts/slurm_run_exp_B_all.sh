#!/bin/bash

CUASpath="$HOME/CUAS"
geompath="$HOME/SHMIP-runs/exp_B_geoms"
resultpath="$HOME/SHMIP-runs/results/B"

mkdir -p $resultpath

run="${CUASpath}/runCUAS.py"
commonpars="--noflownetcdf --dochannels --totaltime 50years --ntout 100 --permeability 0.02 --KChanMax 0.5 --KChanMin 0.003"

parameters=(
            "$run $commonpars $geompath/exp_B1.nc $resultpath/exp_B1_result.nc"
            "$run $commonpars $geompath/exp_B2.nc $resultpath/exp_B2_result.nc"
            "$run $commonpars $geompath/exp_B3.nc $resultpath/exp_B3_result.nc"
            "$run $commonpars $geompath/exp_B4.nc $resultpath/exp_B4_result.nc"
            "$run $commonpars $geompath/exp_B5.nc $resultpath/exp_B5_result.nc"
           )


id=$SLURM_PROCID

echo $id
echo ${parameters[id]}

python -u  ${parameters[id]}


