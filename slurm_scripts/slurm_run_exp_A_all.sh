#!/bin/bash

CUASpath="$HOME/CUAS"
geompath="$HOME/SHMIP-runs/exp_A_geoms"
resultpath="$HOME/SHMIP-runs/results/A"

mkdir -p $resultpath

run="${CUASpath}/runCUAS.py"
commonpars="--noflownetcdf --dochannels --totaltime 50years --ntout 100 --permeability 0.02 --KChanMax 0.5 --KChanMin 0.003"

parameters=(
            "$run $commonpars $geompath/exp_A1.nc $resultpath/exp_A1_result.nc"
            "$run $commonpars $geompath/exp_A2.nc $resultpath/exp_A2_result.nc"
            "$run $commonpars $geompath/exp_A3.nc $resultpath/exp_A3_result.nc"
            "$run $commonpars $geompath/exp_A4.nc $resultpath/exp_A4_result.nc"
            "$run $commonpars $geompath/exp_A5.nc $resultpath/exp_A5_result.nc"
            "$run $commonpars $geompath/exp_A6.nc $resultpath/exp_A6_result.nc"
           )


id=$SLURM_PROCID

echo "SLURM procid: $id"
echo ${parameters[id]}

python -u  ${parameters[id]}


