#!/usr/bin/env python3
'''
'''
import numpy as np
from netCDF4 import Dataset
import argparse
import sys
import datetime

def write_SHMIP_netcdf(filename, x, y, t, runtime, B, H, N, q, K, mask, version):
    '''Write the data to netcdf file'''
    nx = x.size
    ny = y.size
    nnodes = nx * ny # number of nodes

    rows, cols = np.where(mask==1)
    B[rows,cols] = np.nan
    H[rows,cols] = np.nan
    N[:,rows,cols] = np.nan
    q[:,rows,cols] = np.nan
    K[:,rows,cols] = np.nan

    # dimensions
    root_grp = Dataset(filename, 'w', format='NETCDF4')
    root_grp.description = 'CUAS result for SHMIP'
    root_grp.createDimension('time', None)
    root_grp.createDimension('dim', 2)
    root_grp.createDimension('index1', nnodes)

    # variables
    nc_time = root_grp.createVariable('time', 'f8', ('time',))
    nc_time.units = 's'
    nc_time.long_name = 'time'


    nc_coords1 = root_grp.createVariable('coords1', 'f8', ('dim', 'index1'))
    nc_coords1.units = 'm'
    nc_coords1.long_name = 'node coordinates'

    # geometry variables
    nc_B = root_grp.createVariable('B', 'f8', ('index1',))
    nc_B.units = 'm'
    nc_B.long_name = 'bed elevation'
    nc_H = root_grp.createVariable('H', 'f8', ('index1',))
    nc_H.units = 'm'
    nc_H.long_name = 'ice thickness'
    nc_N = root_grp.createVariable('N', 'f8', ('time', 'index1'))
    nc_N.units = 'Pa'
    nc_N.long_name = 'effective pressure'
    nc_q = root_grp.createVariable('q', 'f8', ('time', 'index1'))
    nc_q.units = 'm^2/s'
    nc_q.long_name = 'water sheet discharge'
    nc_K = root_grp.createVariable('K', 'f8', ('time', 'index1'))
    nc_K.units = 'm/s'
    nc_K.long_name = 'conductivity'

    # fill with data
    nc_time[:] = t
    nc_B[:] = np.ascontiguousarray(B)
    nc_H[:] = np.ascontiguousarray(H)
    nc_N[:] = np.ascontiguousarray(N)
    nc_q[:] = np.ascontiguousarray(q)
    nc_K[:] = np.ascontiguousarray(K)

    X,Y = np.meshgrid(x,y)
    nc_coords1[0,:] = X.flatten()
    nc_coords1[1,:] = Y.flatten()

    # attributes
    root_grp.setncattr('title', filename)
    root_grp.setncattr('meshtype', 'structured')
    root_grp.setncattr('channels_on_edges', 'no')
    root_grp.setncattr('institution', 'Sebastian Beyer, PIK/AWI')
    root_grp.setncattr('source', 'CUAS-'+version)
    root_grp.setncattr('dimension', '2D')
    root_grp.setncattr('references', 'http://shmip.bitbucket.io/')
    root_grp.setncattr('runtime', str(datetime.timedelta(seconds=runtime)) )


    root_grp.close()

parser = argparse.ArgumentParser(description='Convert CUAS output to SHMIP format.')
parser.add_argument('cuasfile', metavar='Netcdf_input_file', type=str)
parser.add_argument('shmipfile', metavar='Netcdf_output_file', type=str)
parser.add_argument('--overwriteVersion', metavar='version', type=str, default=None)
parser.add_argument('--starttime', metavar='index', type=int, default=0)
parser.add_argument('--endtime', metavar='index', type=int, default=-1)
parser.add_argument('--reltime', action='store_true')
args = parser.parse_args()

data = Dataset(args.cuasfile, mode='r')
x = data.variables['x'][:]
y = data.variables['y'][:]
topg = data.variables['topg'][:]
thk = data.variables['thk'][:]
time = data.variables['time'][:]
h_time = data.variables['head_t'][:]
N_time = data.variables['peffective_t'][:]
K_time = data.variables['permeability_t'][:]
noflow = data.variables['noflow'][:]
cputime = data.getncattr('cputime')
b = data.getncattr('layerThickness')
version = data.getncattr('version')
data.close()

# select time frame
time = time[args.starttime:args.endtime]
h_time = h_time[args.starttime:args.endtime,:,:]
N_time = N_time[args.starttime:args.endtime,:,:]
K_time = K_time[args.starttime:args.endtime,:,:]

if args.reltime:
    print('unsing relative times')
    time = time - time[0]


dx = x[3]-x[2]
q_time = np.zeros((time.size, y.size, x.size))
for i in range(time.size):
    grad_h_x = np.gradient(h_time[i,:,:])[1]/dx
    grad_h_y = np.gradient(h_time[i,:,:])[0]/dx
    qx = K_time[i,:,:] * b * grad_h_x
    qy = K_time[i,:,:] * b * grad_h_y
    q_mag = np.sqrt(qx**2 + qy**2)
    q_time[i,:,:] = q_mag

if args.overwriteVersion is not None:
    version = args.overwriteVersion

write_SHMIP_netcdf(args.shmipfile, x, y, time, cputime, topg, thk, N_time, q_time, K_time, noflow, version)

