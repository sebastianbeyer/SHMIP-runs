#!/bin/bash

CUASpath="$HOME/playground/CUAS"

mkdir -p ./results/B
mkdir -p ./results/B_plots

nt=1000000
# nt=100
permeability=1.7e-2
permeability=0.0032
nfac=8e7
KChanMax=1.0
rngseed=lama


## B1
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/B/B1_sbey__${permeability}__.nc \
    ./exp_B_geoms/exp_B1.nc \
    ./results/B/exp_B1_result__${permeability}__.nc

## B2
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/B/B2_sbey__${permeability}__.nc \
    ./exp_B_geoms/exp_B2.nc \
    ./results/B/exp_B2_result__${permeability}__.nc

## B3
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/B/B3_sbey__${permeability}__.nc \
    ./exp_B_geoms/exp_B3.nc \
    ./results/B/exp_B3_result__${permeability}__.nc

## B4
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/B/B4_sbey__${permeability}__.nc \
    ./exp_B_geoms/exp_B4.nc \
    ./results/B/exp_B4_result__${permeability}__.nc

## B5
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/B/B5_sbey__${permeability}__.nc \
    ./exp_B_geoms/exp_B5.nc \
    ./results/B/exp_B5_result__${permeability}__.nc


