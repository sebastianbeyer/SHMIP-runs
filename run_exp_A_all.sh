#!/bin/bash

CUASpath="$HOME/playground/CUAS"

mkdir -p ./results/A

nt=1000000
# nt=100
permeability=1.7e-2
permeability=0.0032
nfac=8e7
KChanMax=1.0
rngseed=lama


## A1
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/A/A1_sbey__${permeability}__.nc \
    ./exp_A_geoms/exp_A1.nc \
    ./results/A/exp_A1_result__${permeability}__.nc

## A2
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/A/A2_sbey__${permeability}__.nc \
    ./exp_A_geoms/exp_A2.nc \
    ./results/A/exp_A2_result__${permeability}__.nc

## A3
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/A/A3_sbey__${permeability}__.nc \
    ./exp_A_geoms/exp_A3.nc \
    ./results/A/exp_A3_result__${permeability}__.nc

## A4
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/A/A4_sbey__${permeability}__.nc \
    ./exp_A_geoms/exp_A4.nc \
    ./results/A/exp_A4_result__${permeability}__.nc

## A5
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/A/A5_sbey__${permeability}__.nc \
    ./exp_A_geoms/exp_A5.nc \
    ./results/A/exp_A5_result__${permeability}__.nc

## A6
"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --nt ${nt} \
    --permeability ${permeability} \
    --dochannels \
    --Nfac ${nfac} \
    --KChanMax ${KChanMax} \
    --rngseed ${rngseed} \
    --SHMIPformat \
    ./results/A/A6_sbey__${permeability}__.nc \
    ./exp_A_geoms/exp_A6.nc \
    ./results/A/exp_A6_result__${permeability}__.nc
