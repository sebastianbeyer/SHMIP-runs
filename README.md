Project to make the runs for the SHMIP - Subglacial Hydrology
Model Intercomparison Project.

### runscripts used for the files that were submitted:

#### scripts to make the runs

- `run_exp_A_all.sh`
- `run_exp_B_all.sh`
- `run_exp_E_single.sh` (there was no time for a run all script,
  therefore, this has been used 5 times with varying exp
  parameter)

#### scripts to generate geometry

- `gen_geoms_exp_A.py`
- `gen_geoms_exp_B.py`
- `gen_geoms_exp_E.py`

#### scripts to help with the tuning

- `tune_A.sh`
- `tune_A5.sh`

#### plotscripts

- `compare_GlaDS.py`
- `compare_multi.py`
- `exp_B_plot.py`
- `exp_E_plot.py`
- `toolbox` contains plotting and check scripts from the SHMIP project

#### data

- data for comparison with GlaDS are in `GLADStuning`
- moulin input data are in `moulin_inputs`
