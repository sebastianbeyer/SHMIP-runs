#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset

day = 60 * 60 * 24
tmax = 60 * 60 * 24 * 364 # one year minus one day
nt = 364 # one timestep per hour
t = np.linspace(0, tmax, num=nt)
year = 31536000

lr = -0.0075 # lapse rate (K/m)
DDF = 0.01/86499 # degree day factor (m/K/s)

DTs = np.array([-8.0, -6.0, -4.0, -2.0, 0.0, 2.0, 4.0])

def sqrttopo(x,y):
    '''
    '''
    thick_para = 6.
    thick_off  = 5.0e3
    min_thick  = 1.0
    bed = np.zeros_like(x)
    thickness = min_thick + thick_para * (x + thick_off)**0.5 - 6 * thick_off**0.5
    return bed, thickness


xend = 100.0e3
step = 1.0e3
x = np.arange(-step,xend+step,step)
yend = 20.0e3
y = np.arange(-step, yend+step, step)
X,Y = np.meshgrid(x,y)
topg, thk = sqrttopo(X, Y)

usurf = topg + thk

bnd_mask = np.zeros_like(topg)
# dirichlet / snout
bnd_mask[:,0] = 1
# noflow conditions
bnd_mask[0,:] = 2
bnd_mask[-1,:] = 2
bnd_mask[:,-1] = 2



def write_netcdf(exp):
    DT = DTs[exp-1]
    print('DT=',DT)
    # write netcdf
    filename = './exp_D_geoms/exp_D'+str(exp)+'.nc'
    nx = x.size
    ny = y.size
    # background melting of A1, 7.93e-11 m/s
    runoff_basal = 7.93e-11

    runoff = np.zeros((nt, ny, nx))
    for i in range(0, nt):
        temp = -16 * np.cos(2*np.pi / year * t[i]) - 5 + DT
        runoff[i,:,:] = np.maximum(0, (usurf*lr + temp)*DDF) + runoff_basal

    root_grp = Dataset(filename, 'w', format='NETCDF4')
    root_grp.description = 'sqrt geometry from shmip experiment C' + str(exp)
    root_grp.createDimension('x', nx)
    root_grp.createDimension('y', ny)
    root_grp.createDimension('time', nt)

    # variables
    nc_x = root_grp.createVariable('x', 'f4', ('x',))
    nc_x.units = 'm'
    nc_x.axis = 'X'
    nc_x.long_name = 'X-coordinate in Cartesian system'
    nc_x.standard_name = 'projection_x_coordinate'
    nc_y = root_grp.createVariable('y', 'f4', ('y',))
    nc_y.units = 'm'
    nc_y.axis = 'Y'
    nc_y.long_name = 'Y-coordinate in Cartesian system'
    nc_y.standard_name = 'projection_y_coordinate'
    nc_time = root_grp.createVariable('time', 'f4', ('time',))
    nc_time.units = 's'

    nc_x[:] = x
    nc_y[:] = y
    nc_time[:] = t

    nc_topg = root_grp.createVariable('topg', 'f4', ('y', 'x'))
    nc_topg[:] = topg
    nc_thk = root_grp.createVariable('thk', 'f4', ('y', 'x'))
    nc_thk[:] = thk
    nc_usurf = root_grp.createVariable('usurf', 'f4', ('y', 'x'))
    nc_usurf[:] = usurf

    nc_bmelt = root_grp.createVariable('bmelt', 'f4', ('time', 'y', 'x'))
    nc_bmelt[:] = runoff * 3.154e7 # CUAS expects m/year atm :-(

    nc_bnd_mask = root_grp.createVariable('bnd_mask', 'i2',('y', 'x'))
    nc_bnd_mask[:] = bnd_mask


    root_grp.close()


if __name__ == "__main__":
    for i in range(1,8):
        print('exp=,',i)
        write_netcdf(i)
    # plot3d()

