#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset

def sqrttopo(x,y):
    '''
    '''

    thick_para = 6.
    thick_off  = 5.0e3
    min_thick  = 1.0


    bed = np.zeros_like(x)
    thickness = min_thick + thick_para * (x + thick_off)**0.5 - 6 * thick_off**0.5

    return bed, thickness



xend = 100.0e3
step = 1.0e3
x = np.arange(-step,xend+step,step)
yend = 20.0e3
y = np.arange(-step, yend+step, step)
X,Y = np.meshgrid(x,y)
topg, thk = sqrttopo(X, Y)

usurf = topg + thk

bnd_mask = np.zeros_like(topg)
# dirichlet / snout
bnd_mask[:,0] = 1
# noflow conditions
bnd_mask[0,:] = 2
bnd_mask[-1,:] = 2
bnd_mask[:,-1] = 2


# source terms in m/s
SOURCES = np.array([7.93e-11,
                   1.59e-09,
                   5.79e-09,
                   2.5e-08,
                   4.5e-08,
                   5.79e-07])



def write_netcdf(source, number):


    bmelt = np.ones_like(topg) * source * 3.154e7 # runCUAS expects m/yr atm

    # write netcdf
    filename = './exp_A_geoms/exp_A'+str(number)+'.nc'
    nx = x.size
    ny = y.size

    root_grp = Dataset(filename, 'w', format='NETCDF4')
    root_grp.description = 'sqrt geometry from shmip experiment A' + str(number)
    root_grp.createDimension('x', nx)
    root_grp.createDimension('y', ny)

    # variables
    nc_x = root_grp.createVariable('x', 'f4', ('x',))
    nc_x.units = 'm'
    nc_x.axis = 'X'
    nc_x.long_name = 'X-coordinate in Cartesian system'
    nc_x.standard_name = 'projection_x_coordinate'
    nc_y = root_grp.createVariable('y', 'f4', ('y',))
    nc_y.units = 'm'
    nc_y.axis = 'Y'
    nc_y.long_name = 'Y-coordinate in Cartesian system'
    nc_y.standard_name = 'projection_y_coordinate'

    nc_x[:] = x
    nc_y[:] = y

    nc_topg = root_grp.createVariable('topg', 'f4', ('y', 'x'))
    nc_topg[:] = topg
    nc_thk = root_grp.createVariable('thk', 'f4', ('y', 'x'))
    nc_thk[:] = thk
    nc_usurf = root_grp.createVariable('usurf', 'f4', ('y', 'x'))
    nc_usurf[:] = usurf

    nc_bmelt = root_grp.createVariable('bmelt', 'f4', ('y', 'x'))
    nc_bmelt[:] = bmelt

    nc_bnd_mask = root_grp.createVariable('bnd_mask', 'i2',('y', 'x'))
    nc_bnd_mask[:] = bnd_mask


    root_grp.close()

def plot3d():
    from mpl_toolkits.mplot3d import Axes3D
    from matplotlib import cm
    from matplotlib.ticker import LinearLocator, FormatStrFormatter

    fig = plt.figure(figsize=plt.figaspect(0.5)*1.5)
    ax = fig.gca(projection='3d')
    surf3d = ax.plot_surface(X/1000, Y/1000, thk, rstride=1, cstride=1, cmap=cm.Blues_r,
                       linewidth=0, antialiased=False)
    ax.set_xlabel('x (km)')
    ax.set_ylabel('y (km)')
    ax.set_zlabel('height (m)')
    plt.show()


if __name__ == "__main__":
    i=0
    for source in SOURCES:
        i = i+1
        write_netcdf(source, i)
    # plot3d()

