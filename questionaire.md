# CUAS - Confined Unconfined Aquifer Scheme

The model is similar to the one described in deFleurian et al. 2014[^1] in its
approach to model the subglacial system as an aquifer. The main differences are
that (i) it includes unconfined aquifer areas and (ii) that it only uses a
single layer, representing fast flow regimes (channels) by increasing the
conductivity in these areas. (i) accounts for the system running dry in absence
of sufficient water and (ii) is a simple and efficient way of including both
flow regimes into the model. The model uses a cartesian finite difference grid.
The evolution of the drainage system is done by locally adjusting the
conductivity of the Darcy layer according to opening and closure rates very
similar as described in deFleurian et. al. 2016[^2].


## whole inter-comparison

- Name: CUAS - Confined Unconfined Aquifer Scheme, no publication yet.
- Name of modellers: Sebastian Beyer, Thomas Kleiner, Angelika Humbert
- Computer: "Ollie", Cray CS400, Xeon E5-2697v4 18C 2.3GHz
- Parallel or Serial: serial.
- Availability: not ready yet.
- Version: 0.5.0

## Model tuning

- Model parameters set as given in the parameters section?
  - Physical constants were used as given,
    specific head capacity water and Clausius-Clapeyron constant were not used.
    For the ice flow constant A we used a value of 5e-25 Pa^-3 s^-1, since this is a
    bit of a tuning parameter for our model (see below).
- The model has been tuned to A3 and A5, using N.
  Q and q are of limited use for A5, because the model just has
  a single layer.
- The tuning parameters are the minimum and maximum of the conductivity K_min and K_max,
  which limit the possible state of the drainage system; and also the ice flow constant A
  and the latent heat of fusion, because these govern the opening and closure of the system.
  It sounds a bit strange to use physical constants in order to tune the model, but since
  this is an equivalence model this is the easiest way to do it. In the end, the used parameters
  are very close to literature values anyway: A = 5e-25 Pa^-3 s^-1 and L= 3.34e5 J kg^-1
  (L is exactly the literature value).
  Final values are: K_min = 0.003, K_max = 0.5 m/s.
- additional model parameters:
  - aquifer thickness b = 10m <- this is really also a tuning parameter but we have not yet
    looked at this
  - specific storage Ss = 1e-5m^-1 as in deFleurian et al. 2014[^1]
- for experiments A and B a grid spacing of 1km is used, for
  experiment E and F the spacing is 50m.

## for each suite A-F

### A
- model-time: 50 years
- differing parameters: none
- remarks: none

### B
- model-time: 50 years
- differing parameters: none
- remarks: none

### C
- model-time: 1 year
- differing parameters: none
- remarks: none

### D
- model-time: 10 years
- differing parameters: none
- remarks: none

### E
- model-time: 90 days
- differing parameters: none
- remarks: The very high water input leads to the efficient system
           running into its hard limit (K_max) almost instantly.
           We should re-run this experiment with a higher K_max, but
           I did not have time for that.

### F
- model-time: 1 year
- differing parameters: none
- remarks: The same problem as for E, the system is at maximum
           efficiency already for winter configuration.

## for each run:
cpu time is wall time
  run | cpu time  | converged
      | (minutes) |
 -----+-----------+------------------
  A1  |       14  | yes
  A2  |       36  | yes
  A3  |       67  | yes
  A4  |       72  | yes
  A5  |       76  | yes
  A6  |       76  | yes
 -----+-----------+------------------
  B1  |       77  | yes
  B2  |       76  | yes
  B3  |       76  | yes
  B4  |       74  | yes
  B5  |       74  | yes
 -----+-----------+------------------
  C1  |        2  | yes
  C2  |        2  | yes
  C3  |        2  | yes
  C4  |        1  | yes
 -----+-----------+------------------
  D1  |       13  | yes
  D2  |       14  | yes
  D3  |       15  | yes
  D4  |       15  | yes
  D5  |       15  | yes
 -----+-----------+------------------
  E1  |      178  | yes, see remarks
  E2  |      176  | yes, see remarks
  E3  |      178  | yes, see remarks
  E4  |      176  | yes, see remarks
  E5  |      177  | yes, see remarks
 -----+-----------+------------------
  F1  |           | yes, see remarks
  F2  |           | yes, see remarks
  F3  |           | yes, see remarks
  F4  |           | yes, see remarks
  F5  |           | yes, see remarks




[^1]: de Fleurian, B., Gagliardini, O., Zwinger, T., Durand, G., Le Meur, E., Mair, D., and Råback, P.: A double continuum hydrological model for glacier applications, The Cryosphere, 8, 137-153, doi:10.5194/tc-8-137-2014, 2014.
[^2]: de Fleurian, B., M. Morlighem, H. Seroussi, E. Rignot, M. R. van den Broecke, P. Kuipers Munneke, J. Mouginot, C. J. P. P. Smeets, and A. J. Tedstone (2016), A modeling study of the effect of runoff variability on the effective pressure beneath Russell Glacier, West Greenland, J. Geophys. Res. Earth Surf., 121, 1834–1848, doi:10.1002/2016JF003842
