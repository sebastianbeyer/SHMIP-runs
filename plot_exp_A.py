#!/usr/bin/env python

import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
from netCDF4 import Dataset
import argparse
import sys
import os

parser = argparse.ArgumentParser(description='Compare CUAS output to GlaDS tuning values.')
parser.add_argument('exp_A_result', metavar='result', type=str)
parser.add_argument('output', metavar='output', type=str)
parser.add_argument('--exp', type=int, default=3, help="Experiment, currently 3 or 5.")
args = parser.parse_args()

expA = args.exp_A_result

if (args.exp == 3):
    GLADS_DATA = './GLADStuning/tuning_A3'
elif (args.exp == 5):
    GLADS_DATA = './GLADStuning/tuning_A5'
else:
    print('only experiment A3 or A5 supported.')
    sys.exit(1)


gdata = np.loadtxt(GLADS_DATA, delimiter=',', skiprows=1)

gx = gdata[:,0]
gNmean = gdata[:,1]
gNmin = gdata[:, 2]
gNmax = gdata[:,3]
gQmax = gdata[:,7]


def collapsemean(twodarr):
    mean = np.zeros(twodarr.shape[1])
    for i in range(twodarr.shape[1]):
        mean[i] = np.mean(twodarr[1:-1,i])
    return mean

def readNetcdf(netcdf):
    ncdata = Dataset(netcdf, mode='r')
    ncx = ncdata.variables['x'][:]
    ncy = ncdata.variables['y'][:]
    ncN = ncdata.variables['peffective'][:]
    nceps = ncdata.variables['eps_L2'][:]
    ncK = ncdata.variables['kout'][:]
    ncq = ncdata.variables['flux'][:]

    ncdata.close()

    ncNmean = collapsemean(ncN)
    ncKMean = collapsemean(ncK)
    ncqmean = collapsemean(ncq)

    nx = len(ncx)
    return ncx, ncNmean, nceps, ncKMean, ncqmean



ncx, ncNmean, nceps, ncKmean, ncqmean = readNetcdf(expA)
##### plots ####
# fig = plt.figure(figsize=(12, 6))
f, axarr = plt.subplots(2, sharex=True, figsize=(12,6))

axarr[0].set_title('exp A'+str(args.exp))
axarr[0].plot(gx/1000, gNmean/1e6, label='GlaDS', lw=1.5, color='k')
axarr[0].plot(ncx[:-1]/1000, ncNmean[:-1]/1e6, '--', color='red', label='CUAS', lw=1.4)
axarr[0].set_ylabel('N (MPa)')
axarr[0].set_ylim([0,3.5])
axarr[0].legend(loc='upper left')

axarr[1].plot(ncx[:-1]/1000, ncKmean[:-1], '--', color='red', lw=1.4)
axarr[1].set_ylabel('K (m2/s)')
axarr[1].set_xlabel('x (km)')

plt.savefig(args.output + '/compare_GlaDS_A'+ str(args.exp) +'.png')

