#!/bin/bash

CUASpath="$HOME/playground/CUAS"

mkdir -p ./results/A
mkdir -p ./results/A_plots

"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --dochannels \
    --totaltime 10years \
    --ntout 100 \
    --permeability 0.02 \
    --KChanMax 1.0 \
    --KChanMin 0.001 \
    --layerThickness 10 \
    ./exp_A_geoms/exp_A4.nc \
    ./results/A/exp_A4_result.nc


    # --restart 50d.nc \
    # --permeability 0.0032 \

# compare plot
./compare_GlaDS.py results/A/exp_A5_result.nc results/A_plots --exp 5
