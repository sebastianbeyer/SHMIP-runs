
help:
	@echo "commands:"
	@echo "		geoms_A    - generate geometries for experiment A"
	@echo "		run_A3     - run experiment A3"
	@echo "		geoms_B    - generate geometries for experiment B"
	@echo "		run_B1     - run experiment B1"


geoms_A:
	mkdir -p ./exp_A_geoms
	./gen_geoms_exp_A.py

geoms_B:
	mkdir -p ./exp_B_geoms
	./gen_geoms_exp_B.py

run_A3:
	./run_exp_A3.sh

run_A5:
	./run_exp_A5.sh

run_B1:
	./run_exp_B1.sh

run_all:
	./run_exp_B_all.sh

clean:
	rm -f ./exp_A_geoms/*.nc
	rm -rf ./results/A
	rm -rf ./results/A_plots
	rm -f ./exp_B_geoms/*.nc
	rm -rf ./results/B
	rm -rf ./results/B_plots
