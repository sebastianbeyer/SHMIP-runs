#!/bin/bash

CUASpath="$HOME/playground/CUAS"
exp=5

mkdir -p ./results/E
mkdir -p ./results/E_plots

"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 5 \
    --dochannels \
    --totaltime 1day \
    --KChanMax 1.0 \
    ./exp_E_geoms/exp_E${exp}.nc \
    ./results/E/exp_E${exp}_result.nc

# ./exp_E_plot.py ${exp} ./results/E_plots/E${exp}.png

    # --SHMIPformat \
    # ./results/E/E${exp}_sbey.nc \
