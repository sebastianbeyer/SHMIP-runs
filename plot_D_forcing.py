#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

runoff_basal =  7.93e-11 # m/s
tend = 60 * 60 * 24 *365 # one year
t = np.linspace(0, tend, num=500)
day = 60 * 60 * 24
hour = 60 * 60
year = 31536000

DTs = np.array([-4.0, -2.0, 0.0, 2.0, 4.0])

z_s = 1 # surface elevation
lr = -0.0075 # lapse rate (K/m)
DDF = 0.01/86499 # degree day factor (m/K/s)


for DT in DTs:
    temp = -16 * np.cos(2*np.pi / year * t) - 5 + DT
    runoff = np.maximum(0, (z_s*lr + temp)*DDF) + runoff_basal
    plt.plot(t/day, runoff, label=str(DT))

plt.xlabel('t (h)')
plt.ylabel('runoff multiplier')
plt.legend(title='DT',)
plt.show()
