#!/usr/bin/env python
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import matplotlib as mpl
import datetime

import sys
from argparse import ArgumentParser
from netCDF4 import Dataset

## Set up the option parser
parser = ArgumentParser()
parser.description = '''Plot results form SHMIP experiment C'''
parser.add_argument('ncin')
parser.add_argument('result', default='SHMIP_exp_C.png')
options = parser.parse_args()

## open netcdf file
ncin = Dataset(options.ncin, mode='r')

x = ncin.variables['x'][:]
y = ncin.variables['y'][:]
t = ncin.variables['time'][:]

RHO_I = 910
RHO_W = 1000
G = 9.81

head = ncin.variables['head_t'][:]
N = ncin.variables['peffective_t'][:]
thk  = ncin.variables['thk'][:]
topg = ncin.variables['topg'][:]
K = ncin.variables['permeability_t'][:]
opening = ncin.variables['opening_t'][:]
closure = ncin.variables['closure_t'][:]
eps_L2 = ncin.variables['eps_L2'][:]
eps_inf = ncin.variables['eps_inf'][:]
supply = ncin.variables['watersource_t'][:]
ncin.close()


def spatialmean(variable, location):
    """Mean of variable, locations are
    bottom, middle and top
    """
    locations = {
        'bottom' : (10, 15),
        'middle' : (50, 55),
        'top'    : (85, 90)
        }
    return np.mean(variable[:,:,locations[location][0]:locations[location][1]], axis=(1,2))


nt = head.shape[0]
ny = head.shape[1]
nx = head.shape[2]

xmin= x[0]
xmax= x[-1]
ymin= y[0]
ymax= y[-1]

spy = 60 * 60 * 24 * 365
# time = t / spy
# time = time
t_days = t / (60 * 60 * 24)

Nlongterm = spatialmean(N, 'middle') / 1e6

starttime = 24*333-2
endtime = starttime + 23

N_24 = N[starttime:endtime,:,:]
t_24 = t[starttime:endtime]
t_24_hours = t_24 / (60*60)
N_24_bottom = spatialmean(N_24, 'bottom') / 1e6
N_24_middle = spatialmean(N_24, 'middle') / 1e6
N_24_top = spatialmean(N_24, 'top') / 1e6

supply_24 = supply[starttime:endtime,:,:]
supply_24 = spatialmean(supply_24, 'middle')


plt.figure(figsize=(20,10))
ax1 = plt.subplot2grid((5, 3), (0, 0),  )
ax2 = plt.subplot2grid((5, 3), (0, 1),  )
ax3 = plt.subplot2grid((5, 3), (1, 0), colspan=2)
ax4 = plt.subplot2grid((5, 3), (2, 0), colspan=2)
ax5 = plt.subplot2grid((5, 3), (3, 0),  )
ax6 = plt.subplot2grid((5, 3), (3, 1),  )
ax7 = plt.subplot2grid((5, 3), (4, 0),  )
ax8 = plt.subplot2grid((5, 3), (4, 1),  )

ax1.plot(t_days, Nlongterm)
ax1.set_xlabel('time (days)')
ax1.set_ylabel('N (MPa)')

ax2.plot(t_days, eps_inf, label='eps_inf')
ax2.plot(t_days, eps_L2, label='eps_L2')
ax2.set_ylabel('eps')
ax2.set_xlabel('time (days)')
ax2.set_yscale('log')
ax2.legend()

# ax3.plot(t_24_hours, N_24_bottom, label='bottom')
ax3.plot(t_24_hours, N_24_middle, label='middle')
# ax3.plot(t_24_hours, N_24_top, label='top')
ax3.set_xlabel('time (hours)')
ax3.set_ylabel('N (MPa)')
ax3.legend()

ax4.plot(t_24_hours, supply_24)
ax4.set_xlabel('time (hours)')
ax4.set_ylabel('supply (?)')


lims=(0,2)
normN=mpl.colors.SymLogNorm(linthresh=0.2*np.diff(lims), linscale=2,vmin=lims[0], vmax=lims[1])

im5 = ax5.imshow(N[starttime+6,:,:]/1e6, norm=normN)
ax5.set_title('N day')
ax5.set_xlabel('x (km)')
ax5.set_ylabel('y (km)')
im6 = ax6.imshow(N[starttime+18,:,:]/1e6, norm=normN)
ax6.set_title('N night')
ax6.set_xlabel('x (km)')
ax6.set_ylabel('y (km)')

lims=(0,1)
normK=mpl.colors.SymLogNorm(linthresh=0.2*np.diff(lims), linscale=2,vmin=lims[0], vmax=lims[1])
normK=mpl.colors.SymLogNorm(linthresh=0.05*np.diff(lims), linscale=2,vmin=lims[0], vmax=lims[1])

im7 = ax7.imshow(K[starttime+6,:,:], norm=normK)
ax7.set_title('K day')
ax7.set_xlabel('x (km)')
ax7.set_ylabel('y (km)')
# plt.colorbar(im7, cax=ax7)
im8 = ax8.imshow(K[starttime+18,:,:], norm=normK)
ax8.set_title('K night')
ax8.set_xlabel('x (km)')
ax8.set_ylabel('y (km)')
# plt.colorbar(im8, cax=ax8)

plt.tight_layout()
plt.savefig(options.result, bbox_inches='tight')
