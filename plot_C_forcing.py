#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

runoff_basal =  7.93e-11 # m/s
tend = 60 * 60 * 24 # one day
# tend = 60 * 60 * 24 * 7 # one week
t = np.linspace(0, tend, num=500)
day = 60 * 60 * 24
hour = 60 * 60

ras = np.array([0.25, 0.5, 1, 2])

# runoff(t, ra) = max(0, moulin_in * (1 - ra*sin(2*pi*t/day)))
moulin_in = 1

ra = 0.25

for ra in ras:
    runoff = np.maximum(0, moulin_in * (1 - ra*np.sin(2*np.pi*t/day)))
    plt.plot(t/hour, runoff, label=str(ra))

plt.xlabel('t (h)')
plt.ylabel('runoff multiplier')
plt.legend(title='ra',)
plt.show()
