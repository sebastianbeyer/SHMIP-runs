#!/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset

def valley(x,y,bed_para=None):
    ''' bed, ice_thickness=valley(x,y,bed_para)
    A valley shaped topography with fixed surface in which the bed can be changed (with
    bed_para). It is setup such that changing bed_para does not change the outline.

    Default bed_para=300/6e3 leads to a glacier which mimics Bench-glacier's bed.
    Decreasing it will lead to thicker ice and eventually an overdeepened bed.

    Input:
    x, y coordinates and the bed parameter as defined for the SHMIP exercise, default is 300/6e3

    Output:
    bed and ice thickness in this order

    '''
    # Parameters
    # domain length
    xend = 6.0e3
    # surfaces parameters
    beta = 0.25
    s2 = 100./xend
    min_thick = 1.0
    # bed parameters
    g1 = .5e-6
    alpha = 3.
    bench_para = 300./xend
    if bed_para is None:
        para=bench_para
    else:
        para=bed_para

    # surface
    surf   = 100.*(x+200.)**beta + s2*x - 2.0e10**beta + min_thick
    s_xend = 100.*(xend+200.)**beta + s2*xend - 100.*(200.)**beta + min_thick


    # helper functions
    f_func = para*x + x**2. * (s_xend-para*6.0e3)/6.0e3**2.
    f_Bench = bench_para*x + x**2. * (s_xend-bench_para*6.0e3)/6.0e3**2.
    g_func = 0.5e-6 * abs(y)**3.
    h_func = (5 - 4.5*x/6.0e3) * (surf-f_func)/(surf-f_Bench)

    #base

    bed=f_func+ g_func * h_func

    thickness =surf-bed
    thickness[np.where(thickness<0)]=0.0

    return bed, thickness


def valley_outline(step):
    '''Defines the outline of the valley glacier used in the SHMIP exercise
    Generation of the outline is done with a step of 10 m per default
    Input:
    x coordinates (vector with the x coordinate of all points)
    Output:
    returns X and Y in this order

    '''
    # domain length
    xend = 6.0e3
    x = np.arange(0,xend+step,step)

    # surf para
    beta = 0.25
    s1 = 100.0
    s2 = 100.0/xend
    sx0 = -200.
    s3 = 0.

    # bed para
    g1 = 0.5e-6
    alpha = 3.

    s_xend = s1*(xend-sx0)**beta + s2*xend - s1*(-sx0)**beta + s3
    surf = s1*(x-sx0)**beta + s2*x - s1*(-sx0)**beta + s3
    # bed:

    f20 = 300./xend
    f10 = (s_xend - f20*xend)/xend**2
    f0 = f10*x**2 + f20*x
    h0 = -4.5*(x/xend) + 5

    y = (((surf-f0)/h0)/g1)**(1/alpha)

    #mirroring to get the positive and negative Y side
    YPos=np.hstack((y,-y[::-1]))
    XPos=np.hstack((x,x[::-1]))

    #return XPos, YPos
    return x,y

def plot_E():
    topgma = np.ma.array(topg, mask=mask)
    thkma = np.ma.array(thk, mask=mask)


    plt.subplot(411)
    plt.imshow(topgma, interpolation='None', extent=[x.min(), x.max(), y.min(), y.max()])
    plt.plot(xo, yo, color='k')
    plt.subplot(412)
    plt.imshow(thkma, interpolation='None', extent=[x.min(), x.max(), y.min(), y.max()])
    plt.subplot(413)
    plt.plot(xo, yo)
    plt.subplot(414)
    plt.imshow(temp_pa, interpolation='None', extent=[x.min(), x.max(), y.min(), y.max()])

    plt.show()

def write_netcdf(ncfile, geompara, DT):
    xend = 6.0e3
    step=50.0

    x = np.arange(0,xend+step,step)
    yend = 550.
    y = np.arange(-yend, yend+step, step)
    X,Y = np.meshgrid(x,y)
    nx = x.size
    ny = y.size

    topg, thk = valley(X, Y, geompara)

# outline
    xo, yo = valley_outline(step)

# the +5 is necessary because the outline does not really fit!
    yind = np.rint(-yo/step).astype(int) + int(y.size/2)# + 5
    xind = (x/step).astype(int)

#topg[yind,xind] = -200
    mask = np.zeros_like(topg, dtype=bool)
    for i in xind:
        mask[:yind[i],i] = 1
        #mask[yind[i],i] = 1

# mirroring the mask
    mask = mask | mask[::-1,:]

    usurf = topg + thk

#bmelt
    bmelt = np.ones_like(topg) * 1.158e-6 * 3.154e7 # in m/year
    # background melting of A1, 7.93e-11 m/s
    runoff_basal = 7.93e-11
    runoff = np.zeros((nt, ny, nx))
    for i in range(0, nt):
        temp = -16 * np.cos(2*np.pi / year * t[i]) - 5 + DT
        runoff[i,:,:] = np.maximum(0, (usurf*lr + temp)*DDF) + runoff_basal
    #bnd_mask
    bnd_mask = np.zeros_like(topg)
    bnd_mask[mask] = 2

    bnd_mask[int(y.size/2)-4:int(y.size/2)+5,0] = 1
    bnd_mask[0,:] = 2
    bnd_mask[-1,:] = 2
    bnd_mask[:,-1] = 2


# write netcdf
    filename = ncfile
    root_grp = Dataset(filename, 'w', format='NETCDF4')
    root_grp.description = 'valley geometry from shmip'
    root_grp.createDimension('x', nx)
    root_grp.createDimension('y', ny)
    root_grp.createDimension('time', nt)
# variables
    nc_x = root_grp.createVariable('x', 'f4', ('x',))
    nc_x.units = 'm'
    nc_x.axis = 'X'
    nc_x.long_name = 'X-coordinate in Cartesian system'
    nc_x.standard_name = 'projection_x_coordinate'
    nc_y = root_grp.createVariable('y', 'f4', ('y',))
    nc_y.units = 'm'
    nc_y.axis = 'Y'
    nc_y.long_name = 'Y-coordinate in Cartesian system'
    nc_y.standard_name = 'projection_y_coordinate'
    nc_x[:] = x
    nc_y[:] = y
    nc_time = root_grp.createVariable('time', 'f4', ('time',))
    nc_time.units = 's'
    nc_time[:] = t
    nc_topg = root_grp.createVariable('topg', 'f4', ('y', 'x'))
    nc_topg[:] = topg
    nc_thk = root_grp.createVariable('thk', 'f4', ('y', 'x'))
    nc_thk[:] = thk
    nc_usurf = root_grp.createVariable('usurf', 'f4', ('y', 'x'))
    nc_usurf[:] = usurf

    nc_bmelt = root_grp.createVariable('bmelt', 'f4', ('time', 'y', 'x'))
    nc_bmelt[:] = runoff * 3.154e7 # CUAS expects m/year atm :-(

    nc_bnd_mask = root_grp.createVariable('bnd_mask', 'i2',('y', 'x'))
    nc_bnd_mask[:] = bnd_mask

    root_grp.close()


if __name__ == "__main__":
    import os
    path = "./exp_F_geoms/"
    os.makedirs(path, exist_ok=True)

    day = 60 * 60 * 24
    tmax = 60 * 60 * 24 * 364 # one year minus one day
    nt = 364 # one timestep per hour
    t = np.linspace(0, tmax, num=nt)
    year = 31536000
    lr = -0.0075 # lapse rate (K/m)
    DDF = 0.01/86499 # degree day factor (m/K/s)

    geomparam = 0.05
    DTs = np.array([-6.0, -3.0, 0.0, 3.0, 6.0])

    for i in range(1,6):
        print(DTs[i-1])
        filename = path+'exp_F'+str(i)+'.nc'
        write_netcdf(filename, geomparam, DTs[i-1])


