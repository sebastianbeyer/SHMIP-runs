#!/bin/bash

CUASpath="$HOME/playground/CUAS"

mkdir -p ./results/B
mkdir -p ./results/B_plots

exp=5

"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --downsample 1 \
    --totaltime '20 years' \
    --permeability 0.017 \
    --dochannels \
    --KChanMax 1.0 \
    ./exp_B_geoms/exp_B${exp}.nc \
    ./results/B/exp_B${exp}_result.nc

./exp_B_plot.py ${exp} ./results/B_plots/B${exp}.png
