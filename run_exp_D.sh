#!/bin/bash

CUASpath="$HOME/playground/CUAS"

mkdir -p ./results/D
mkdir -p ./results/D_plots

"${CUASpath}"/runCUAS.py \
    --noflownetcdf \
    --dochannels \
    --restart ./results/A/exp_A1_result_for_D.nc \
    --totaltime 10years \
    --ntout 3650 \
    --permeability 0.02 \
    --KChanMax 0.2 \
    --layerThickness 10 \
    ./exp_D_geoms/exp_D1.nc \
    ./results/D/exp_D1_result.nc


# compare plot
# ./compare_GlaDS.py results/A/exp_A5_result.nc results/A_plots --exp 5
