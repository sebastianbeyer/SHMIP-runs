#!/usr/bin/env python
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation

import sys
from argparse import ArgumentParser
from netCDF4 import Dataset

## Set up the option parser
parser = ArgumentParser()
parser.description = '''Plot of experiment'''
parser.add_argument('experiment')
parser.add_argument('plotout')
args = parser.parse_args()

ncin = 'results/B/exp_B'+str(args.experiment)+'_result.nc'


## open netcdf file
ncin = Dataset(ncin, mode='r')

x = ncin.variables['x'][:]
y = ncin.variables['y'][:]
t = ncin.variables['time'][:]

RHO_I = 920
RHO_W = 1000
G = 9.81

head = ncin.variables['head_t'][:]
thk  = ncin.variables['thk'][:]
topg = ncin.variables['topg'][:]
K = ncin.variables['permeability_t'][:]
flux = ncin.variables['flux'][:]
ncin.close()

nt = head.shape[0]
ny = head.shape[1]
nx = head.shape[2]

xmin= x[0]
xmax= x[nx-1]
ymin= y[0]
ymax= y[ny-1]

pcmap=plt.cm.get_cmap('RdBu_r',20)


pw = (head - topg) * RHO_W * G
pi = thk * RHO_I * G
pe = (pi - pw) / 1e6  # in MPa

vmin=0.003
vmax=0.3

hmin=0
hmax=1300

pmin=-3
pmax=3

qmin=0.001
qmax=0.1

cmap=plt.cm.get_cmap('viridis',20)

fig, (ax1, ax2, ax3, ax4) = plt.subplots(4,1,figsize=[16,12])

ax1.axes.set_aspect('equal')
ax1.set_xlabel(r'x in km')
ax1.set_ylabel(r'y in km')
ax2.axes.set_aspect('equal')
ax2.set_xlabel(r'x in km')
ax2.set_ylabel(r'y in km')
ax3.axes.set_aspect('equal')
ax3.set_xlabel(r'x in km')
ax3.set_ylabel(r'y in km')
ax4.axes.set_aspect('equal')
ax4.set_xlabel(r'x in km')
ax4.set_ylabel(r'y in km')
im1 = ax1.imshow(head[90,:,:], interpolation='none', origin='lower', vmin=hmin, vmax=hmax, cmap=cmap)
plt.colorbar(im1, ax=ax1, label='h (m)')
im2 = ax2.imshow(pe[90,:,:], interpolation='none', origin='lower', vmin=pmin, vmax=pmax, cmap=pcmap)
plt.colorbar(im2, ax=ax2, label='Pe (MPa)')
import matplotlib.colors as colors
im3 = ax3.imshow(K[90,:,:], interpolation='none', origin='lower',  cmap=cmap,extent=[0,100,0,20], norm=colors.LogNorm(vmin=0.001, vmax=0.3))
plt.colorbar(im3, ax=ax3, label='K (m2/s)')

exp=args.experiment
moulins = np.loadtxt('./moulin_inputs/B'+str(exp)+'_M.csv', delimiter=',', ndmin=2)
for row in moulins:
    m_x = row[1]
    m_y = row[2]
    m_input = row[3]
    ax3.plot(m_x/1000, m_y/1000,marker=(5,2),color='k')

# flux
cmap=plt.cm.get_cmap('magma',20)
im4 = ax4.imshow(flux, interpolation='none', origin='lower', cmap=cmap,extent=[0,100,0,20], norm=colors.LogNorm(vmin=qmin, vmax=qmax) )
plt.colorbar(im4, ax=ax4, label='q (m2/s or m3/s)')


fig.savefig(args.plotout, bbox_inches='tight')

