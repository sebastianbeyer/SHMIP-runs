#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset

def sqrttopo(x,y):
    '''
    '''
    thick_para = 6.
    thick_off  = 5.0e3
    min_thick  = 1.0
    bed = np.zeros_like(x)
    thickness = min_thick + thick_para * (x + thick_off)**0.5 - 6 * thick_off**0.5
    return bed, thickness


xend = 100.0e3
step = 1.0e3
x = np.arange(-step,xend+step,step)
yend = 20.0e3
y = np.arange(-step, yend+step, step)
X,Y = np.meshgrid(x,y)
topg, thk = sqrttopo(X, Y)

usurf = topg + thk
bnd_mask = np.zeros_like(topg)
# dirichlet / snout
bnd_mask[:,0] = 1
# noflow conditions
bnd_mask[0,:] = 2
bnd_mask[-1,:] = 2
bnd_mask[:,-1] = 2


def nearest_gridpoint(array, value):
    ''' Find nearest gridpoint to a certain value
        This assumes that coordinates in array
        describe the midpoint of a cell.
        Returns the index of the corresponding cell.
    '''
    index_array = (np.abs(array - value)).argmin()
    return index_array

def add_moulin(m_x, m_y, m_input):
    '''
    '''
    # print(m_x, m_y, m_input)
# convert input (m^3/s to m/s)
    m_input_ms = m_input / step**2
    cell_x = nearest_gridpoint(x, m_x)
    cell_y = nearest_gridpoint(y, m_y)

    moulin_onArray = np.zeros_like(topg)
    moulin_onArray[cell_y, cell_x] = m_input_ms * 3.154e7 # m/yr
    return moulin_onArray


def write_netcdf(exp, number):
    # background melting of A1, 7.93e-11 m/s
    bmelt = np.ones_like(topg) * 7.93e-11 * 3.154e7 # runCUAS expects m/yr atm

    moulins = np.loadtxt('./moulin_inputs/B'+str(exp)+'_M.csv', delimiter=',', ndmin=2)

    moulins_2d = np.zeros_like(topg)
    for row in moulins:
        m_x = row[1]
        m_y = row[2]
        m_input = row[3]
        moulins_2d = moulins_2d + add_moulin(m_x, m_y, m_input)
    bmelt = bmelt + moulins_2d


    # write netcdf
    filename = './exp_B_geoms/exp_B'+str(number)+'.nc'
    nx = x.size
    ny = y.size

    root_grp = Dataset(filename, 'w', format='NETCDF4')
    root_grp.description = 'sqrt geometry from shmip experiment A' + str(number)
    root_grp.createDimension('x', nx)
    root_grp.createDimension('y', ny)

    # variables
    nc_x = root_grp.createVariable('x', 'f4', ('x',))
    nc_x.units = 'm'
    nc_x.axis = 'X'
    nc_x.long_name = 'X-coordinate in Cartesian system'
    nc_x.standard_name = 'projection_x_coordinate'
    nc_y = root_grp.createVariable('y', 'f4', ('y',))
    nc_y.units = 'm'
    nc_y.axis = 'Y'
    nc_y.long_name = 'Y-coordinate in Cartesian system'
    nc_y.standard_name = 'projection_y_coordinate'

    nc_x[:] = x
    nc_y[:] = y

    nc_topg = root_grp.createVariable('topg', 'f4', ('y', 'x'))
    nc_topg[:] = topg
    nc_thk = root_grp.createVariable('thk', 'f4', ('y', 'x'))
    nc_thk[:] = thk
    nc_usurf = root_grp.createVariable('usurf', 'f4', ('y', 'x'))
    nc_usurf[:] = usurf

    nc_bmelt = root_grp.createVariable('bmelt', 'f4', ('y', 'x'))
    nc_bmelt[:] = bmelt

    nc_bnd_mask = root_grp.createVariable('bnd_mask', 'i2',('y', 'x'))
    nc_bnd_mask[:] = bnd_mask


    root_grp.close()



if __name__ == "__main__":
    for i in range(1,6):
        write_netcdf(i, i)
    # plot3d()

