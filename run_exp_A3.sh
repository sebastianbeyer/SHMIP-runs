#!/bin/bash

CUASpath="$HOME/playground/CUAS"

mkdir -p ./results/A
mkdir -p ./results/A_plots

${CUASpath}/runCUAS.py \
    --noflownetcdf \
    --dochannels \
    --totaltime 20years \
    --ntout 100 \
    --permeability 0.02 \
    --KChanMax 0.2 \
    --layerThickness 10 \
    ./exp_A_geoms/exp_A3.nc \
    ./results/A/exp_A3_result.nc

    # --totaltime 1000000000 \

# compare plot
./compare_GlaDS.py results/A/exp_A3_result.nc results/A_plots
